#include "bmp/bmp.h"
#include "img/image.h"
#include "op/rotate.h"
#include <stdio.h>


int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return -1;
    }

    struct image source = { 0 };
    enum read_status rs = from_bmp_name(argv[1], &source);

    if (rs != READ_OK) {
        fprintf(stderr, "COULD NOT READ BMP, ERROR: %d\n", rs);
        free_image(&source);
        return rs;
    }

    int w_status = 0;

    struct image dest = rotate_image(source);
    enum write_status ws = to_bmp_name(argv[2], &dest);

    if (ws != WRITE_OK) {
        fprintf(stderr, "COULD NOT WRITE BMP, ERROR: %d\n", ws);
        w_status = ws;
    }

    free_image(&source);
    free_image(&dest);

    return w_status;
}
