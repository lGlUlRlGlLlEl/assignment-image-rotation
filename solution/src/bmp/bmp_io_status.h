#ifndef BMP_IO_STATUS_H
#define BMP_IO_STATUS_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_PIXELS_ERROR,
    WRITE_ERROR
};

#endif
