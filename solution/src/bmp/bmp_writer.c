#include "bmp.h"
#include "bmp_writer.h"

enum write_status write_bmp_header(FILE *out, struct image *image, struct bmp_header *header) {
    if (!out || !image)
        return WRITE_ERROR;

    init_bmp_header(header, image);

    if (!fwrite(header,sizeof(struct bmp_header),1,out))
        return WRITE_HEADER_ERROR;

    return WRITE_OK;
}

enum write_status write_bmp_pixels(FILE *out, struct image *image) {
    if (!out || !image)
        return WRITE_ERROR;

    const size_t width = get_width(image);
    const size_t height = get_height(image);
    const uint8_t padding = get_padding(width);

    struct pixel *pixels = get_pixels(image);

    for (size_t i = 0; i < height; i++) {
        if (fwrite(pixels, sizeof (struct pixel), width, out) != width)
            return WRITE_PIXELS_ERROR;
        if (fwrite(pixels, 1, padding, out) != padding)
            return WRITE_PIXELS_ERROR;

        pixels += (size_t) width;
    }

    return WRITE_OK;
}
